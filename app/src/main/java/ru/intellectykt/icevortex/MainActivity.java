package ru.intellectykt.icevortex;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void didRegister(View view) {
        Intent ToRegisterActivityIntent= new Intent(this,RegisterActivity.class);
        startActivity(ToRegisterActivityIntent);
    }

    public void didauth(View view) {
        Intent ToRegisterActivityIntent= new Intent(this,AuthActivity.class);
        startActivity(ToRegisterActivityIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences sp = getSharedPreferences("data", MODE_PRIVATE);
        if (sp.contains("access_token") && sp.contains("inn")){
            Intent ToRegisterActivityIntent= new Intent(this,ProfileActivity.class);
            startActivity(ToRegisterActivityIntent);
        }


    }
}
