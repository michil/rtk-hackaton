package ru.intellectykt.icevortex;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ChooseTarifsActivity extends AppCompatActivity {

    ListView listView;
    ArrayList <Tarif> tarifs = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_tarifs);
        listView=findViewById(R.id.Tarifs_list_view);
        Gettarifs();


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                SetTarif(tarifs.get(position));
            }
        });

    }


    void SetTarif(Tarif tarif){
        SharedPreferences sp = getSharedPreferences("data", MODE_PRIVATE);

        AndroidNetworking.post("http://icevortex.intellectykt.ru/API/settarif.php")
                .addBodyParameter("INN", sp.getString("inn", ""))
                .addBodyParameter("ACCESS_TOKEN", sp.getString("access_token", ""))
                .addBodyParameter("TARIF_ID", String.valueOf(tarif.getId()))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println(response);
                        try {
                            Boolean result=response.getBoolean("result");

                            if (result){
                                Toast.makeText(ChooseTarifsActivity.this,"Тариф успешно сменен", Toast.LENGTH_LONG ).show();


                            }else{
                                Toast.makeText(ChooseTarifsActivity.this,response.getString("message"), Toast.LENGTH_LONG ).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(ChooseTarifsActivity.this,anError.getErrorDetail(), Toast.LENGTH_LONG ).show();
                    }
                });
    }

    void Gettarifs(){
        AndroidNetworking.post("http://icevortex.intellectykt.ru/API/gettarifs.php")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println(response);
                        try {
                            Boolean result=response.getBoolean("result");

                            if (result){
                                JSONArray tarifs_json=response.getJSONArray("Tarifs");

                                for (int i = 0; i <tarifs_json.length() ; i++) {
                                    Integer id=tarifs_json.getJSONObject(i).getInt("ID");
                                    String name=tarifs_json.getJSONObject(i).getString("NAME");
                                    String speed=tarifs_json.getJSONObject(i).getString("SPEED");

                                    Tarif tarif = new Tarif(id,name,speed);
                                    tarifs.add(tarif);


                                }

                                TarifsAdapter adapter = new TarifsAdapter(ChooseTarifsActivity.this, tarifs);
                                listView.setAdapter(adapter);

                            }else{
                                Toast.makeText(ChooseTarifsActivity.this,response.getString("message"), Toast.LENGTH_LONG ).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(ChooseTarifsActivity.this,anError.getErrorDetail(), Toast.LENGTH_LONG ).show();
                    }
                });
    }
}
