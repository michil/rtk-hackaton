package ru.intellectykt.icevortex;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import org.json.JSONArray;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class TarifsAdapter extends BaseAdapter {
    private Activity context;
    private ArrayList<Tarif> tarifs;



    // Constructor
    public TarifsAdapter(Activity context, ArrayList<Tarif> tarifs) {
        this.context = context;
        this.tarifs = tarifs;
    }

    public int getCount() {
        return tarifs.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.tarif_cell, parent, false);



            TextView tarif = rowView.findViewById(R.id.tarif_cell_tarif);
            TextView speed = rowView.findViewById(R.id.tarif_cell_speed);





            tarif.setText("Тариф: " + tarifs.get(position).getName());
        speed.setText("Тариф скорость: " + tarifs.get(position).getSpeed() + " кб в секунду");





        return rowView;
    }

}
