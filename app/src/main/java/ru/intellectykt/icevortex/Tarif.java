package ru.intellectykt.icevortex;

public class Tarif {

    Integer id;
    String name;
    String speed;

    public Tarif(Integer id, String name, String speed) {
        this.id = id;
        this.name = name;
        this.speed = speed;
    }


    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSpeed() {
        return speed;
    }
}
