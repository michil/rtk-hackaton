package ru.intellectykt.icevortex;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

public class AuthActivity extends AppCompatActivity {

    EditText inn;
    EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        initView();
    }


    void initView(){
        inn = (EditText) findViewById(R.id.auth_inn);
        password = (EditText) findViewById(R.id.auth_password);
    }

    public void auth(View view) {
        AndroidNetworking.post("http://icevortex.intellectykt.ru/API/auth.php")
                .addBodyParameter("INN", inn.getText().toString())
                .addBodyParameter("PASS",password.getText().toString() )
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println(response);
                        try {
                            Boolean result=response.getBoolean("result");

                            if (result){
                                SharedPreferences sp = getSharedPreferences("data", MODE_PRIVATE);
                                SharedPreferences.Editor editor = sp.edit();

                                editor.putString("access_token", response.getString("ACCESS_TOKEN"));
                                editor.putString("inn", inn.getText().toString());

                                editor.apply();

                                finish();


                            }else{
                                Toast.makeText(AuthActivity.this,response.getString("message"), Toast.LENGTH_LONG ).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(AuthActivity.this,anError.getErrorDetail(), Toast.LENGTH_LONG ).show();
                    }
                });
    }
}
