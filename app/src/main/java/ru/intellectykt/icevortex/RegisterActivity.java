package ru.intellectykt.icevortex;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity {

    EditText Fname;
    EditText Lname;
    EditText Patronim;
    EditText Born_Date;
    EditText inn;
    EditText pasport;
    EditText password1;
    EditText password2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initView();
    }


    void initView(){
        Fname = (EditText) findViewById(R.id.firstname);
        Lname = (EditText) findViewById(R.id.lastname);
        Patronim = (EditText) findViewById(R.id.patrionim);
        Born_Date = (EditText) findViewById(R.id.borndate);
        inn = (EditText) findViewById(R.id.inn);
        pasport = (EditText) findViewById(R.id.pasport);
        password1 = (EditText) findViewById(R.id.password1);
        password2 = (EditText) findViewById(R.id.password2);
    }

    public void register(View view) {

        if (!password2.getText().toString().equals(password1.getText().toString())){
            password2.setError("пароль не совпадает");
        }else{

            AndroidNetworking.post("http://icevortex.intellectykt.ru/API/addusr.php")
                    .addBodyParameter("FIRST_NAME", Fname.getText().toString())
                    .addBodyParameter("LAST_NAME",Lname.getText().toString() )
                    .addBodyParameter("PATRIONIM",Patronim.getText().toString() )
                    .addBodyParameter("BORN_DATE",Born_Date.getText().toString())
                    .addBodyParameter("INN",inn.getText().toString())
                    .addBodyParameter("PASS",password1.getText().toString() )
                    .addBodyParameter("PASSPORT",pasport.getText().toString())
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            System.out.println(response);
                            try {
                                Boolean result=response.getBoolean("result");

                                if (result){
                                    Toast.makeText(RegisterActivity.this,"Регистрация прошла успешно", Toast.LENGTH_LONG ).show();
                                    finish();
                                }else{
                                    Toast.makeText(RegisterActivity.this,response.getString("message"), Toast.LENGTH_LONG ).show();

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Toast.makeText(RegisterActivity.this,anError.getLocalizedMessage(), Toast.LENGTH_LONG ).show();
                        }
                    });

        }

    }
}
