package ru.intellectykt.icevortex;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

public class ProfileActivity extends AppCompatActivity {

    TextView Fname;
    TextView Lname;
    TextView Patronim;
    TextView Born_date;
    TextView inn;
    TextView passport;
    TextView tarif_name;
    TextView tarif_speed;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        get_profile();


    }

    void initView(){
        Fname=findViewById(R.id.profile_FIRST_NAME);
        Lname=findViewById(R.id.profile_LAST_NAME);
        Patronim=findViewById(R.id.profile_PATRIONIM);
        Born_date=findViewById(R.id.profile_BORN_DATE);
        inn=findViewById(R.id.profile_inn);
        passport=findViewById(R.id.profile_passport);
        tarif_name=findViewById(R.id.profile_TARIF);
        tarif_speed=findViewById(R.id.profile_TARIF_SPEED);
    }

    void get_profile(){
        SharedPreferences sp = getSharedPreferences("data", MODE_PRIVATE);

        AndroidNetworking.post("http://icevortex.intellectykt.ru/API/getuser.php")
                .addBodyParameter("INN", sp.getString("inn", ""))
                .addBodyParameter("ACCESS_TOKEN", sp.getString("access_token", ""))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println(response);
                        try {
                            Boolean result=response.getBoolean("result");

                            if (result){
                                Fname.setText("Фамилия: " + response.getJSONObject("User").getString("FIRST_NAME"));
                                Lname.setText("Имя: " + response.getJSONObject("User").getString("LAST_NAME"));
                                Patronim.setText("Отчество: " + response.getJSONObject("User").getString("PATRIONIM"));
                                Born_date.setText("Дата рождения: " + response.getJSONObject("User").getString("BORN_DATE"));
                                inn.setText("ИНН: " + response.getJSONObject("User").getString("INN"));
                                passport.setText("Паспорт: " + response.getJSONObject("User").getString("PASSPORT"));
                                tarif_name.setText("Тариф: " + response.getJSONObject("User").getJSONObject("TARIF").getString("NAME"));
                                tarif_speed.setText("Тариф скорость: " + response.getJSONObject("User").getJSONObject("TARIF").getString("SPEED") + " кб в секунду");


                            }else{
                                Toast.makeText(ProfileActivity.this,response.getString("message"), Toast.LENGTH_LONG ).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(ProfileActivity.this,anError.getErrorDetail(), Toast.LENGTH_LONG ).show();
                    }
                });
    }

    public void logaut(View view) {
        SharedPreferences sp = getSharedPreferences("data", MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        editor.remove("access_token");
        editor.remove("inn");

        editor.apply();
        finish();
    }

    public void chooseTarifs(View view) {
        Intent ToRegisterActivityIntent= new Intent(this, ChooseTarifsActivity.class);
        startActivity(ToRegisterActivityIntent);
    }
}
